public class HelloWorld {
    private String name;
    private Greetings greetings;

    public void setName(String name) {
        this.name = name;
    }

    public Greetings getGreetings() {
        return greetings;
    }

    public void setGreetings(Greetings greetings) {
        this.greetings = greetings;
    }

    public void printHello() {
        System.out.println("Hello ! " + name);
    }
}
